import 'package:app/services/api.service.dart';
import 'package:app/services/category.service.dart';
import 'package:app/services/implementation/api.service.impl.dart';
import 'package:app/services/implementation/category.service.impl.dart';
import 'package:app/services/implementation/item.service.impl.dart';
import 'package:app/services/implementation/user.service.impl.dart';
import 'package:app/services/item.service.dart';
import 'package:app/services/user.service.dart';
import 'package:app/views/home/home.model.dart';
import 'package:app/views/single-category/single_category.model.dart';
import 'package:get_it/get_it.dart';

GetIt locator = GetIt.instance;

void setupLocator() {
  locator.registerLazySingleton<ApiService>(() => ApiServiceImplementation());
  locator.registerLazySingleton<UserService>(() => UserServiceImplementation());
  locator.registerLazySingleton<CategoryService>(
      () => CategoryServiceImplementation());
  locator.registerLazySingleton<ItemService>(() => ItemServiceImplementation());

  locator.registerFactory(() => SingleCategoryModel());
  locator.registerFactory(() => HomeModel());
}
