import 'package:app/common/helper.function.dart';
import 'package:app/di.dart';
import 'package:app/size_config.dart';
import 'package:app/theme.dart';
import 'package:app/views/home/home.view.dart';
import 'package:flutter/material.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();

  // Setup the dependency injection
  setupLocator();

  runApp(const BookmarkApp());
}

class BookmarkApp extends StatelessWidget {
  const BookmarkApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    HelperFunction.updateStatusBarColor(null);
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: getThemeData(context),
      builder: (context, child) {
        SizeConfig().init(context);
        return child!;
      },
      home: HomeView(),
    );
  }
}
