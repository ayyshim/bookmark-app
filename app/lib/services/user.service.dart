import 'package:app/models/user.model.dart';

abstract class UserService {
  UserModel get user;

  Future<void> initUser();
}
