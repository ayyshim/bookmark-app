import 'package:dio/dio.dart';

abstract class ApiService {
  /// Send [GET] request to the server and returns <T> object
  Future<Response> get(String point, {Map<String, dynamic>? params});

  /// Send [PUT] request to the server and returns <T> object
  Future<Response> put(String point, {Map<String, dynamic> body});

  /// Send [DELETE] request to the server and returns [bool]
  Future<bool> delete(String point);
}
