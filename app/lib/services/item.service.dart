import 'package:app/models/item.model.dart';
import 'package:app/models/label.model.dart';

enum Action { add, remove }

abstract class ItemService {
  Future<List<ItemModel>> getItems();

  Future<bool> deleteItem(int id);

  Future<List<LabelModel>> getLabels();

  
  Future<bool> bookmark(Action action, {ItemModel item});

  Future<bool> favorite(Action action, {ItemModel item});
}
