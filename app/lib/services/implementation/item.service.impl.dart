import 'package:app/constants.dart';
import 'package:app/di.dart';
import 'package:app/models/item.model.dart';
import 'package:app/models/label.model.dart';
import 'package:app/services/api.service.dart';
import 'package:app/services/category.service.dart';
import 'package:app/services/item.service.dart';

class ItemServiceImplementation implements ItemService {
  final ApiService _apiService = locator<ApiService>();

  @override
  Future<bool> deleteItem(int id) {
    try {
      return _apiService.delete(Constants.get.modify(itemId: id));
    } catch (e) {
      rethrow;
    }
  }

  @override
  Future<List<ItemModel>> getItems() async {
    try {
      final List<ItemModel> items = [];
      final response = await _apiService.get(Constants.get.items, params: {
        'catId': locator<CategoryService>().activeCategory,
      });

      if (response.data != null) {
        final data = List<Map<String, dynamic>>.from(response.data as List);

        for (final Map<String, dynamic> element in data) {
          items.add(ItemModel.fromJson(element));
        }
      }

      return items;
    } catch (e) {
      rethrow;
    }
  }

  @override
  Future<List<LabelModel>> getLabels() async {
    try {
      final List<LabelModel> labels = [];
      final response = await _apiService.get(Constants.get.labels, params: {
        'catId': locator<CategoryService>().activeCategory,
      });

      if (response.data != null) {
        final data = List<Map<String, dynamic>>.from(response.data as List);

        for (final Map<String, dynamic> element in data) {
          labels.add(LabelModel.fromJson(element));
        }
      }

      return labels;
    } catch (e) {
      rethrow;
    }
  }

  @override
  Future<bool> bookmark(Action action, {ItemModel? item}) async {
    try {
      ItemModel updatedItem;
      if (action == Action.add) {
        updatedItem = item!.copyWith(isBookmarked: true);
      } else {
        updatedItem = item!.copyWith(isBookmarked: false);
      }

      final response = await _apiService.put(
        Constants.get.modify(itemId: item.id),
        body: updatedItem.toJson(),
      );

      return response.statusCode == 200;
    } catch (e) {
      rethrow;
    }
  }

  @override
  Future<bool> favorite(Action action, {ItemModel? item}) async {
    try {
      ItemModel updatedItem;
      if (action == Action.add) {
        updatedItem = item!.copyWith(isFavorite: true);
      } else {
        updatedItem = item!.copyWith(isFavorite: false);
      }

      final response = await _apiService.put(
        Constants.get.modify(itemId: item.id),
        body: updatedItem.toJson(),
      );

      return response.statusCode == 200;
    } catch (e) {
      rethrow;
    }
  }
}
