import 'package:app/constants.dart';
import 'package:app/di.dart';
import 'package:app/models/user.model.dart';
import 'package:app/services/api.service.dart';
import 'package:app/services/user.service.dart';

class UserServiceImplementation implements UserService {
  final ApiService _apiService = locator<ApiService>();

  late UserModel? _user;
  @override
  UserModel get user => _user!;

  @override
  Future<void> initUser() async {
    try {
      final response = await _apiService.get(Constants.get.userDetails);
      if (response.data != null) {
        final data = List<Map<String, dynamic>>.from(response.data as List);
        _user = UserModel.fromJson(data[0]);
      }
    } catch (e) {
      rethrow;
    }
  }
}
