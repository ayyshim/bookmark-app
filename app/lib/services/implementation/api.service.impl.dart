import 'package:app/constants.dart';
import 'package:app/services/api.service.dart';
import 'package:dio/dio.dart';

class ApiServiceImplementation implements ApiService {
  final Dio _dio = Dio(BaseOptions(
    baseUrl: Constants.get.host,
    contentType: 'application/json',
  ));

  @override
  Future<Response> get(String point, {Map<String, dynamic>? params}) {
    try {
      return _dio.get(point, queryParameters: params);
    } catch (e) {
      rethrow;
    }
  }

  @override
  Future<Response> put(String point, {Map<String, dynamic>? body}) {
    try {
      return _dio.put(point, data: body);
    } catch (e) {
      rethrow;
    }
  }

  @override
  Future<bool> delete(String point) async {
    try {
      final response = await _dio.delete(point);
      return response.statusCode == 200;
    } catch (e) {
      rethrow;
    }
  }
}
