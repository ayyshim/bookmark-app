import 'package:app/constants.dart';
import 'package:app/di.dart';
import 'package:app/models/category.model.dart';
import 'package:app/services/api.service.dart';
import 'package:app/services/category.service.dart';

class CategoryServiceImplementation implements CategoryService {
  final ApiService _apiService = locator<ApiService>();

  late int? _activeCategory;
  @override
  int get activeCategory => _activeCategory!;

  @override
  void setActiveCategory(int id) {
    _activeCategory = id;
  }

  @override
  Future<List<CategoryModel>> getCategories() async {
    try {
      final List<CategoryModel> categories = [];

      final response = await _apiService.get(Constants.get.categories);

      if (response.data != null) {
        final data = List<Map<String, dynamic>>.from(response.data as List);

        for (final Map<String, dynamic> element in data) {
          categories.add(CategoryModel.fromJson(element));
        }
      }

      return categories;
    } catch (e) {
      rethrow;
    }
  }
}
