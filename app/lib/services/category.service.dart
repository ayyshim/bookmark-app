import 'package:app/models/category.model.dart';

abstract class CategoryService {
  int get activeCategory;

  void setActiveCategory(int id);

  Future<List<CategoryModel>> getCategories();
}
