class Constants {
  static Constants get = Constants();

  // 🖇️API constants
  String get host =>
      "https://9b8d1ac2ebbc.ngrok.io"; // <- Put the base URL here without trailing slash

  // 🔗 Endpoints
  String get userDetails => "$host/userDetails";
  String get categories => "$host/categories";

  /// 💡 Add ?catId={id} at the end of this url to get the items of a category
  String get items => "$host/items";

  /// 💡 Add ?catId={id} at the end of this url to get the labels of a category
  String get labels => "$host/labels";

  /// 💡 This endpoint is used to make PUT/DELETE requests to the server.
  String modify({int? itemId}) {
    return "$host/items/$itemId";
  }
}
