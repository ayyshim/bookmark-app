class LabelModel {
  final int id, catId;
  final String name;

  LabelModel._({
    required this.id,
    required this.catId,
    required this.name,
  });

  factory LabelModel.fromJson(Map<String, dynamic> json) {
    return LabelModel._(
      id: json['id'] as int,
      catId: json['catId'] as int,
      name: json['name'] as String,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'catId': catId,
      'name': name,
    };
  }
}
