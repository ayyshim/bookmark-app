class ItemModel {
  final int id, labelId, catId;
  final String image, title;

  bool _isFavorite = false, _isBookmarked = false;

  void toggleFavorite() {
    _isFavorite = !_isFavorite;
  }

  void toggleBookmark() {
    _isBookmarked = !_isBookmarked;
  }

  bool get isFavorite => _isFavorite;
  bool get isBookmarked => _isBookmarked;

  ItemModel._({
    required this.id,
    required this.labelId,
    required this.catId,
    required this.image,
    required this.title,
    required bool? isBookmarked,
    required bool? isFavorite,
  }) {
    _isBookmarked = isBookmarked!;
    _isFavorite = isFavorite!;
  }

  factory ItemModel.fromJson(Map<String, dynamic> json) {
    return ItemModel._(
      id: json['id'] as int,
      labelId: json['labelId'] as int,
      catId: json['catId'] as int,
      image: json['image'] as String,
      title: json['title'] as String,
      isBookmarked: json['isBookmarked'] as bool,
      isFavorite: json['isFavorite'] as bool,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'labelId': labelId,
      'catId': catId,
      'image': image,
      'title': title,
      'isBookmarked': isBookmarked,
      'isFavorite': isFavorite,
    };
  }

  ItemModel copyWith({
    int? labelId,
    int? catId,
    String? image,
    String? title,
    bool? isBookmarked,
    bool? isFavorite,
  }) {
    return ItemModel._(
      id: id,
      labelId: labelId ?? this.labelId,
      catId: catId ?? this.catId,
      image: image ?? this.image,
      title: title ?? this.title,
      isBookmarked: isBookmarked ?? _isBookmarked,
      isFavorite: isFavorite ?? _isFavorite,
    );
  }
}
