class UserModel {
  final int id;
  final String fullName;

  UserModel._({required this.id, required this.fullName});

  factory UserModel.fromJson(Map<String, dynamic> json) {
    return UserModel._(
      id: json["id"] as int,
      fullName: json["fullName"] as String,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'fullName': fullName,
    };
  }
}
