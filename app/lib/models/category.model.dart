

class CategoryModel {
  final int id, totalBookmarked;
  final String category, icon;

  CategoryModel._({
    required this.id,
    required this.category,
    required this.totalBookmarked,
    required this.icon,
  });

  factory CategoryModel.fromJson(Map<String, dynamic> json) {
    return CategoryModel._(
      id: json["id"] as int,
      category: json["category"] as String,
      totalBookmarked: json["total_bookmarked"] as int,
      icon: "assets/categories/${json["category"]}.svg",
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'category': category,
      'total_bookmarked': totalBookmarked,
      'icon': icon,
    };
  }
}
