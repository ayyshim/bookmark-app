import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class AppColors {
  static AppColors get = AppColors();

  Color get scaffold => const Color(0xFFF8F8F8);
  Color get primary => const Color(0xFFFF903F);
  Color get inputField => const Color(0xFFFEDBC2).withOpacity(0.56);
  Color get activeChip => primary;
  Color get inactiveChip => const Color(0xFFFEBA88).withOpacity(0.6);
  Color get green => const Color(0xFF2CF3A0);
  Color get unsetIcons => const Color(0xFFF9D0B2).withOpacity(0.72);
  Color get setIcons => green;
  Color get titleText => const Color(0xFF605151);
  Color get red => const Color(0xFFFF7070);

  Map<String, Map<String, Color>> get categoryColors => {
        "New Ideas": {
          "background": const Color(0xFFF1EFB7),
          "foreground": const Color(0xFFCDBD2B),
        },
        "Cooking": {
          "background": const Color(0xFFFBCFCF),
          "foreground": const Color(0xFFFF7070),
        },
        "Programming": {
          "background": const Color(0xFFB7F1D9),
          "foreground": const Color(0xFF2BCD89),
        },
        "Music": {
          "background": const Color(0xFFCFEBFB),
          "foreground": const Color(0xFF7087FF),
        },
      };
}

ThemeData? getThemeData(BuildContext context) {
  return ThemeData(
    scaffoldBackgroundColor: AppColors.get.scaffold,
    primaryColor: AppColors.get.primary,
    accentColor: AppColors.get.green,
    appBarTheme: AppBarTheme(
      centerTitle: true,
      backgroundColor: AppColors.get.primary,
      elevation: 0,
    ),
    textTheme: GoogleFonts.mPlusRounded1cTextTheme(
      Theme.of(context).textTheme,
    ),
  );
}
