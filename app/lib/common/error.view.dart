import 'package:app/size_config.dart';
import 'package:app/theme.dart';
import 'package:flutter/material.dart';

class ErrorView extends StatelessWidget {
  final String errorMessage;
  final VoidCallback? onRetry;

  const ErrorView({
    Key? key,
    required this.errorMessage,
    this.onRetry,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          body: Padding(
        padding: const EdgeInsets.all(24),
        child: SizedBox(
          height: double.infinity,
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(
                  child: Image.asset("assets/ErrorLamp.png"),
                ),
                Text(
                  errorMessage,
                  textAlign: TextAlign.center,
                ),
                const SizedBox(
                  height: 10,
                ),
                SizedBox(
                  width: SizeConfig.screenWidth! / 2,
                  child: ElevatedButton(
                    onPressed: onRetry,
                    style: ElevatedButton.styleFrom(
                      primary: AppColors.get.primary,
                      onPrimary: Colors.white,
                      elevation: 0,
                    ),
                    child: const Text("Retry"),
                  ),
                ),
              ],
            ),
          ),
        ),
      )),
    );
  }
}
