import 'package:app/theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class HelperFunction {
  /// Update the color of status bar.
  /// Pass [Color] to change accordingly or if null,
  /// [primary] color will be set.
  static void updateStatusBarColor(Color? color) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: color ?? AppColors.get.primary,
    ));
  }
}
