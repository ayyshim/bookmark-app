import 'package:app/common/enum/view_state.enum.dart';
import 'package:flutter/cupertino.dart';

mixin ViewModelMixin {
  ViewState _state = ViewState.normal;
  ViewState get state => _state;

  void updateState({
    required ViewState state,
    VoidCallback? cb,
  }) {
    _state = state;
    cb!();
  }
}
