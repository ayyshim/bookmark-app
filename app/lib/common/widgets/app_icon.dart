import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class AppIcon extends StatelessWidget {
  final String name;
  final double size;
  final Color color;

  AppIcon(
      {Key? key, required this.name, this.size = 18, this.color = Colors.white})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SvgPicture.asset(
      "assets/icons/$name.svg",
      height: size,
      width: size,
      color: color,
    );
  }
}
