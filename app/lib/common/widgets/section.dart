import 'package:app/size_config.dart';
import 'package:app/theme.dart';
import 'package:flutter/material.dart';

class Section extends StatelessWidget {
  final String title;
  final double spaceBetweenTitleAndContent;
  final Widget child;

  Section({
    Key? key,
    required this.title,
    required this.child,
    this.spaceBetweenTitleAndContent = 14,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 14),
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          Text(
            title,
            style: Theme.of(context).textTheme.bodyText1?.copyWith(
                  color: AppColors.get.titleText,
                  fontSize: getProportionateScreenWidth(19),
                  fontWeight: FontWeight.w700,
                ),
          ),
          SizedBox(
            height: spaceBetweenTitleAndContent,
          ),
          child,
        ],
      ),
    );
  }
}
