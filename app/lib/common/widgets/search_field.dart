import 'package:app/size_config.dart';
import 'package:app/theme.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class SearchField extends StatelessWidget {
  SearchField({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextField(
      decoration: InputDecoration(
        contentPadding:
            const EdgeInsets.symmetric(horizontal: 16, vertical: 14),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(100.0),
          borderSide: BorderSide.none,
        ),
        filled: true,
        hintStyle: GoogleFonts.openSans(
          textStyle: TextStyle(
            color: Colors.white,
            fontSize: getProportionateScreenWidth(12),
            fontWeight: FontWeight.w600,
          ),
        ),
        hintText: "What bookmarks are you searching for ?",
        fillColor: AppColors.get.inputField,
        prefixIcon: Icon(
          Icons.search_rounded,
          color: Colors.white,
          size: getProportionateScreenWidth(22),
        ),
      ),
    );
  }
}
