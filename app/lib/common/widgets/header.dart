import 'package:app/common/widgets/search_field.dart';
import 'package:app/common/widgets/toolbar.widget.dart';
import 'package:app/size_config.dart';
import 'package:app/theme.dart';
import 'package:app/views/home/widgets/greeting.widget.dart';
import 'package:flutter/material.dart';

class Header extends StatelessWidget {
  final String? title;
  final bool notificationBadge;
  final bool isHome;
  final bool showBackButton;
  final Color? backgroundColor;

  Header({
    Key? key,
    this.title,
    this.notificationBadge = false,
    this.isHome = false,
    this.showBackButton = true,
    this.backgroundColor,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 14, vertical: 16),
      height: getProportionateScreenHeight(152),
      decoration: BoxDecoration(
          color: backgroundColor ?? AppColors.get.primary,
          borderRadius: BorderRadius.only(
            bottomRight: Radius.circular(getProportionateScreenHeight(34)),
            bottomLeft: Radius.circular(getProportionateScreenHeight(34)),
          )),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisSize: MainAxisSize.min,
        children: [
          Toolbar(
            isHome: isHome,
            title: title,
            notificationBadge: notificationBadge,
            showBackButton: showBackButton,
          ),
          if (isHome) const Greeting(),
          if (!isHome)
            SizedBox(
              height: getProportionateScreenWidth(18 + 22),
            ),
          const SizedBox(height: 14),
          SearchField(),
        ],
      ),
    );
  }
}
