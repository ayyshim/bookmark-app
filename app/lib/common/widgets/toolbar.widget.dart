import 'package:app/common/widgets/app_icon.dart';
import 'package:app/size_config.dart';
import 'package:app/theme.dart';
import 'package:flutter/material.dart';

class Toolbar extends StatelessWidget {
  final String? title;
  final bool notificationBadge;
  final bool isHome;
  final bool showBackButton;

  const Toolbar({
    Key? key,
    this.title,
    this.isHome = false,
    this.notificationBadge = false,
    this.showBackButton = true,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        if (Navigator.canPop(context) && showBackButton)
          InkWell(
            onTap: () {
              Navigator.pop(context);
            },
            child: const Icon(
              Icons.arrow_back_ios_new_sharp,
              color: Colors.white,
            ),
          ),
        if (title != null)
          Text(
            title!,
            style: Theme.of(context).textTheme.bodyText1?.copyWith(
                  color: Colors.white,
                  fontSize: getProportionateScreenWidth(22),
                  fontWeight: FontWeight.w700,
                ),
            textAlign: TextAlign.center,
          ),
        if (isHome) const Spacer(),
        Stack(children: [
          InkWell(
            onTap: () {},
            child: AppIcon(
              name: "Notification",
            ),
          ),
          if (notificationBadge)
            Positioned(
              right: 0,
              child: Container(
                height: 8,
                width: 8,
                decoration: BoxDecoration(
                  color: AppColors.get.green,
                  borderRadius: BorderRadius.circular(4),
                ),
              ),
            )
        ])
      ],
    );
  }
}
