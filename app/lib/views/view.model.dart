import 'package:app/common/enum/view_state.enum.dart';
import 'package:flutter/cupertino.dart';

class ViewModel extends ChangeNotifier {
  // [States and State manager start]
  final Map<String, bool> _busyWidgetsMap = {};

  ViewState _state = ViewState.normal;
  ViewState get state => _state;
  void setState(ViewState viewState) {
    // _busyWidgetsMap.entries.forEach((element) {
    //   _busyWidgetsMap[element.key] = false;
    // });
    _state = viewState;
    notifyListeners();
  }

  late String _errorMessage;
  String get errorMessage => _errorMessage;

  late String _warningMessage;
  String get warningMessage => _warningMessage;

  void setErrorMessage(String msg) {
    _errorMessage = msg;
    setState(ViewState.error);
  }

  void setLoading() {
    setState(ViewState.busy);
  }

  void setIdle() {
    setState(ViewState.normal);
  }

  // Use [setWidgetBusy(), unsetWidgetBusy()] if you want to only show progress indicator to
  // certain widget.
  void setWidgetBusy(String key) {
    if (_busyWidgetsMap.containsKey(key)) {
      _busyWidgetsMap.update(key, (_) => true);
    } else {
      _busyWidgetsMap.putIfAbsent(key, () => true);
    }
    notifyListeners();
  }

  void unsetWidgetBusy(String key) {
    if (_busyWidgetsMap.containsKey(key)) {
      _busyWidgetsMap.update(key, (_) => false);
    } else {
      _busyWidgetsMap.putIfAbsent(key, () => false);
    }
    notifyListeners();
  }

  // This getter function will return a boolean value which will
  // indicate if the model state is busy or not.
  bool get isBusy => _state == ViewState.busy;

  bool isBusyWidget(String key) => _busyWidgetsMap[key] ?? false;

  // [States and State manager end]

  // [Model disposal/reuse logic start]
  bool _onModelReadyCalled = false;
  bool get onModelReadyCalled => _onModelReadyCalled;
  void updateOnModelReadyCalled({bool? status}) {
    _onModelReadyCalled = status!;
  }

  // [Model disposal/reuse logic end]

}
