import 'package:app/common/enum/view_state.enum.dart';
import 'package:app/common/error.view.dart';
import 'package:app/common/helper.function.dart';
import 'package:app/common/widgets/app_icon.dart';
import 'package:app/common/widgets/header.dart';
import 'package:app/common/widgets/section.dart';
import 'package:app/models/category.model.dart';
import 'package:app/size_config.dart';
import 'package:app/theme.dart';
import 'package:app/views/base.view.dart';
import 'package:app/views/home/home.model.dart';
import 'package:app/views/single-category/single_category.view.dart';
import 'package:app/views/single-category/widgets/rounded_icon_butto.widget.dart';
import 'package:flutter/material.dart';

class HomeView extends StatefulWidget {
  @override
  _HomeViewState createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> {
  @override
  Widget build(BuildContext context) {
    return BaseView<HomeModel>(
      onModelReady: (model) {
        model.init();
      },
      enableTouchRepeal: true,
      builder: (context, model, child) {
        if (model.state == ViewState.error) {
          return ErrorView(
            errorMessage: model.errorMessage,
            onRetry: model.init,
          );
        }

        return SafeArea(
          child: Scaffold(
            body: model.isBusy
                ? const Center(
                    child: CircularProgressIndicator(),
                  )
                : SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Header(
                          isHome: true,
                          notificationBadge: true,
                          showBackButton: false,
                        ),
                        Section(
                          title: "Categories",
                          child: GridView.count(
                            shrinkWrap: true,
                            crossAxisCount: 2,
                            mainAxisSpacing: 17,
                            crossAxisSpacing: 13,
                            children: model.categories
                                .map(
                                  (category) => _categoryCard(category),
                                )
                                .toList(),
                          ),
                        ),
                      ],
                    ),
                  ),
            bottomNavigationBar: model.isBusy
                ? null
                : Stack(
                    clipBehavior: Clip.none,
                    children: [
                      Container(
                        height: getProportionateScreenHeight(50),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            boxShadow: [
                              BoxShadow(
                                offset: const Offset(0, 5),
                                blurRadius: 15,
                                spreadRadius: 5,
                                color: Colors.black.withOpacity(0.15),
                              )
                            ],
                            borderRadius: const BorderRadius.only(
                              topLeft: Radius.circular(14),
                              topRight: Radius.circular(14),
                            )),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            AppIcon(
                              name: "Home",
                              color: Colors.red,
                              size: 24,
                            ),
                            AppIcon(
                              name: "Favorites",
                              color: Colors.red,
                              size: 24,
                            ),
                          ],
                        ),
                      ),
                      Positioned(
                        top: -26,
                        left: (SizeConfig.screenWidth! / 2) - 26,
                        child: RoundedIconButton(
                          size: 52,
                          iconName: "AddItem",
                          onPressed: () {},
                        ),
                      ),
                    ],
                  ),
          ),
        );
      },
    );
  }

  Widget _categoryCard(CategoryModel category) {
    final Color _backgroundColor =
        AppColors.get.categoryColors[category.category]!["background"]!;
    final Color _foregroundColor =
        AppColors.get.categoryColors[category.category]!["foreground"]!;

    return Material(
      color: _backgroundColor,
      borderRadius: BorderRadius.circular(21),
      clipBehavior: Clip.antiAlias,
      child: InkWell(
        splashColor: _backgroundColor.withOpacity(0.5),
        onTap: () async {
          await Navigator.push(context, MaterialPageRoute(
            builder: (context) {
              return SingleCategoryView(
                category: category,
              );
            },
          ));
          HelperFunction.updateStatusBarColor(null);
        },
        child: Container(
          padding: const EdgeInsets.only(top: 20, bottom: 15),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                height: getProportionateScreenWidth(72),
                width: getProportionateScreenWidth(72),
                decoration: BoxDecoration(
                  color: Colors.white.withOpacity(0.73),
                  borderRadius: BorderRadius.circular(36),
                ),
                child: Center(
                  child: AppIcon(
                    name: "categories/${category.category}",
                    size: 32,
                    color: _foregroundColor,
                  ),
                ),
              ),
              const SizedBox(
                height: 7,
              ),
              Text(
                category.category,
                style: Theme.of(context).textTheme.bodyText1?.copyWith(
                      fontSize: getProportionateScreenWidth(14),
                      fontWeight: FontWeight.w700,
                      color: _foregroundColor,
                    ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
