import 'package:app/di.dart';
import 'package:app/models/category.model.dart';
import 'package:app/models/user.model.dart';
import 'package:app/services/category.service.dart';
import 'package:app/services/user.service.dart';
import 'package:app/views/view.model.dart';
import 'package:flutter/foundation.dart';
import 'package:fluttertoast/fluttertoast.dart';

class HomeModel extends ViewModel {
  final UserService _userService = locator<UserService>();
  final CategoryService _categoryService = locator<CategoryService>();

  UserModel get currentUser => _userService.user;

  List<CategoryModel> _categories = [];
  List<CategoryModel> get categories => _categories;

  Future<void> init() async {
    setLoading();
    try {
      await _userService.initUser();
      _categories = await _categoryService.getCategories();
      setIdle();
    } catch (e) {
      debugPrint(e.toString());
      Fluttertoast.showToast(
        msg: "Opps! That's awkward. Something went wrong!",
      );
      setErrorMessage(
          "It could be either problem in network or my code. Either way, it's awkward.");
    }
  }
}
