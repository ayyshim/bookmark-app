import 'package:app/di.dart';
import 'package:app/services/user.service.dart';
import 'package:app/size_config.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class Greeting extends StatelessWidget {
  const Greeting({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final String userName = locator<UserService>().user.fullName.split(" ")[0];

    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 11),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "Hi $userName",
            style: GoogleFonts.openSans(
              textStyle: TextStyle(
                color: Colors.white,
                fontSize: getProportionateScreenWidth(18),
                fontWeight: FontWeight.w600,
              ),
            ),
          ),
          Text(
            "Welcome Back !!!",
            style: Theme.of(context).textTheme.bodyText1?.copyWith(
                  color: Colors.white,
                  fontSize: getProportionateScreenWidth(22),
                  fontWeight: FontWeight.w700,
                ),
          ),
        ],
      ),
    );
  }
}
