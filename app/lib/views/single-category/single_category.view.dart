import 'dart:ui';

import 'package:app/common/enum/view_state.enum.dart';
import 'package:app/common/error.view.dart';
import 'package:app/common/helper.function.dart';
import 'package:app/common/widgets/app_icon.dart';
import 'package:app/common/widgets/header.dart';
import 'package:app/common/widgets/section.dart';
import 'package:app/models/category.model.dart';
import 'package:app/models/item.model.dart';
import 'package:app/size_config.dart';
import 'package:app/theme.dart';
import 'package:app/views/base.view.dart';
import 'package:app/views/single-category/single_category.model.dart';
import 'package:app/views/single-category/widgets/bookmark_header.widget.dart';
import 'package:app/views/single-category/widgets/labels.widget.dart';
import 'package:app/views/single-category/widgets/rounded_icon_butto.widget.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class SingleCategoryView extends StatefulWidget {
  final CategoryModel category;

  SingleCategoryView({required this.category});

  @override
  _SingleCategoryViewState createState() => _SingleCategoryViewState();
}

class _SingleCategoryViewState extends State<SingleCategoryView> {
  late Color? _headerBackgroundColor;

  @override
  Widget build(BuildContext context) {
    return BaseView<SingleCategoryModel>(
      enableTouchRepeal: true,
      onModelReady: (model) {
        _headerBackgroundColor = AppColors
            .get.categoryColors[widget.category.category]!["foreground"];

        HelperFunction.updateStatusBarColor(_headerBackgroundColor);
        model.init(widget.category);
      },
      builder: (context, model, child) {
        if (model.state == ViewState.error) {
          return ErrorView(
            errorMessage: model.errorMessage,
            onRetry: () => model.init(widget.category),
          );
        }

        return SafeArea(
          child: Scaffold(
            body: model.isBusy
                ? const Center(
                    child: CircularProgressIndicator(),
                  )
                : Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Header(
                        title: widget.category.category,
                        backgroundColor: _headerBackgroundColor,
                      ),
                      Expanded(
                        child: Section(
                          title: "Labels",
                          spaceBetweenTitleAndContent: 9,
                          child: Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              children: [
                                Labels(
                                  items: model.labels,
                                  onItemPressed: model.onLabelPressed,
                                  position: model.activeLabelIndex,
                                ),
                                const SizedBox(
                                  height: 16,
                                ),
                                BookmarkHeader(
                                  totalBookmarked: model.totalBookmarks,
                                ),
                                const SizedBox(
                                  height: 18,
                                ),
                                _buildItems(model),
                                const SizedBox(
                                  height: 18,
                                ),
                              ],
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
          ),
        );
      },
    );
  }

  Widget _buildItems(SingleCategoryModel model) {
    if (model.itemsToShow.isEmpty) {
      return Expanded(
        child: Center(
          child: Text(
            "No Bookmarks Found!",
            style: GoogleFonts.openSans(
              textStyle: TextStyle(
                fontWeight: FontWeight.w400,
                color: AppColors.get.unsetIcons,
                fontSize: getProportionateScreenWidth(18),
              ),
            ),
          ),
        ),
      );
    } else {
      return Expanded(
        child: ListView.separated(
          separatorBuilder: (context, index) => const SizedBox(
            height: 18,
          ),
          itemBuilder: (context, index) {
            final item = model.itemsToShow[index];
            return _buildItem(
              model.itemsToShow[index],
              onBookmarkPressed: model.toggleBookmarkItem,
              onFavoritePressed: model.toggleFavoriteItem,
              onDeletePressed: model.deleteItem,
              isBookmarkBusy: model.isBusyWidget('item-bookmark-${item.id}'),
              isFavoriteBusy: model.isBusyWidget('item-favorite-${item.id}'),
              isDeleteInProcess: model.isBusyWidget('item-delete-${item.id}'),
            );
          },
          itemCount: model.itemsToShow.length,
          shrinkWrap: true,
        ),
      );
    }
  }

  Widget _buildItem(
    ItemModel item, {
    ValueSetter<ItemModel>? onFavoritePressed,
    ValueSetter<ItemModel>? onBookmarkPressed,
    ValueSetter<ItemModel>? onDeletePressed,
    bool isBookmarkBusy = false,
    bool isFavoriteBusy = false,
    bool isDeleteInProcess = false,
  }) {
    return AbsorbPointer(
      absorbing: isDeleteInProcess,
      child: Opacity(
        opacity: isDeleteInProcess ? 0.5 : 1,
        child: Material(
          color: Colors.white,
          borderRadius: BorderRadius.circular(15),
          child: Container(
            padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 12),
            child: Row(
              children: [
                Container(
                  height: 58,
                  width: 60,
                  clipBehavior: Clip.antiAlias,
                  decoration: BoxDecoration(
                    color: Colors.black12,
                    borderRadius: BorderRadius.circular(9),
                  ),
                  child: Image.network(
                    item.image,
                    fit: BoxFit.cover,
                  ),
                ),
                const SizedBox(
                  width: 10,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      item.title,
                      style: Theme.of(context).textTheme.bodyText1?.copyWith(
                            color: AppColors.get.titleText,
                            fontWeight: FontWeight.w700,
                            fontSize: getProportionateScreenWidth(15),
                          ),
                    ),
                    const SizedBox(
                      height: 8,
                    ),
                    Row(
                      children: [
                        if (isBookmarkBusy)
                          SizedBox(
                            height: 18,
                            width: 18,
                            child: CircularProgressIndicator(
                              strokeWidth: 1,
                              valueColor: AlwaysStoppedAnimation<Color>(
                                AppColors.get.green,
                              ),
                            ),
                          )
                        else
                          GestureDetector(
                            onTap: () => onBookmarkPressed!(item),
                            child: AppIcon(
                              name: "Notify",
                              color: item.isBookmarked
                                  ? AppColors.get.setIcons
                                  : AppColors.get.unsetIcons,
                            ),
                          ),
                        const SizedBox(
                          width: 9,
                        ),
                        if (isFavoriteBusy)
                          SizedBox(
                            height: 18,
                            width: 18,
                            child: CircularProgressIndicator(
                              strokeWidth: 1,
                              valueColor: AlwaysStoppedAnimation<Color>(
                                AppColors.get.green,
                              ),
                            ),
                          )
                        else
                          GestureDetector(
                            onTap: () => onFavoritePressed!(item),
                            child: AppIcon(
                              name: "Favorite",
                              color: item.isFavorite
                                  ? AppColors.get.setIcons
                                  : AppColors.get.unsetIcons,
                            ),
                          ),
                      ],
                    ),
                  ],
                ),
                const Spacer(),
                RoundedIconButton(
                  iconName: "Open",
                  onPressed: () {},
                  backgroundColor: AppColors.get.green,
                ),
                const SizedBox(
                  width: 8,
                ),
                RoundedIconButton(
                  iconName: "Trash",
                  onPressed: () => onDeletePressed!(item),
                  backgroundColor: AppColors.get.red,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
