import 'package:app/common/widgets/app_icon.dart';
import 'package:app/theme.dart';
import 'package:flutter/material.dart';

class RoundedIconButton extends StatelessWidget {
  final String iconName;
  final Color? backgroundColor;
  final Color? iconColor;
  final double? iconSize;
  final double size;
  final VoidCallback? onPressed;

  const RoundedIconButton({
    Key? key,
    required this.iconName,
    this.backgroundColor,
    this.iconColor,
    this.onPressed,
    this.size = 36,
    this.iconSize,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      color: backgroundColor ?? AppColors.get.primary,
      borderRadius: BorderRadius.circular(100),
      clipBehavior: Clip.antiAlias,
      child: InkWell(
        onTap: onPressed,
        child: Container(
          padding: const EdgeInsets.all(8),
          height: size,
          width: size,
          child: AppIcon(
            name: iconName,
            color: iconColor ?? Colors.white,
            size: iconSize ?? 18,
          ),
        ),
      ),
    );
  }
}
