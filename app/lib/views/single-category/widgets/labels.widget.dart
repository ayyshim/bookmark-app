import 'package:app/common/widgets/horizontal_list.widget.dart';
import 'package:app/models/label.model.dart';
import 'package:app/views/single-category/widgets/label.widget.dart';
import 'package:flutter/material.dart';

class Labels extends StatelessWidget {
  final List<LabelModel> items;
  final Function(int, int) onItemPressed;
  final int position;

  const Labels({
    Key? key,
    required this.items,
    required this.onItemPressed,
    this.position = 0,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return HorizontalList(
      itemCount: items.length,
      fillerCount: 0,
      seprator: (context, index) => const SizedBox(
        width: 6,
      ),
      itemBuilder: (context, index) {
        final label = items[index];
        return Label(
          label: label,
          active: index == position,
          onPressed: (value) => onItemPressed(value, index),
        );
      },
    );
  }
}
