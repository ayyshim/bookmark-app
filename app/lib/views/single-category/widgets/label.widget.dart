import 'package:app/models/label.model.dart';
import 'package:app/size_config.dart';
import 'package:app/theme.dart';
import 'package:flutter/material.dart';

class Label extends StatelessWidget {
  final LabelModel label;
  final bool active;
  final ValueSetter<int> onPressed;

  const Label({
    Key? key,
    required this.label,
    required this.onPressed,
    this.active = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ActionChip(
      elevation: 0,
      pressElevation: 0,
      labelStyle: Theme.of(context).textTheme.bodyText1?.copyWith(
            fontWeight: FontWeight.w700,
            color: Colors.white,
            fontSize: getProportionateScreenWidth(12),
          ),
      backgroundColor:
          active ? AppColors.get.activeChip : AppColors.get.inactiveChip,
      label: Text(label.name),
      onPressed: () {
        onPressed(label.id);
      },
    );
  }
}
