import 'package:app/common/widgets/app_icon.dart';
import 'package:app/size_config.dart';
import 'package:app/theme.dart';
import 'package:flutter/material.dart';

class BookmarkHeader extends StatelessWidget {
  final int totalBookmarked;

  const BookmarkHeader({Key? key, required this.totalBookmarked})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "Bookmarks",
              style: Theme.of(context).textTheme.bodyText1?.copyWith(
                    fontWeight: FontWeight.w700,
                    fontSize: getProportionateScreenWidth(19),
                    color: AppColors.get.titleText,
                  ),
            ),
            const SizedBox(
              height: 3,
            ),
            Row(
              children: [
                AppIcon(
                  name: "Bookmark",
                  color: AppColors.get.primary,
                ),
                const SizedBox(
                  width: 5,
                ),
                Text(
                  "$totalBookmarked",
                  style: Theme.of(context).textTheme.bodyText1?.copyWith(
                        color: AppColors.get.primary,
                        fontSize: getProportionateScreenWidth(11),
                        fontWeight: FontWeight.w400,
                      ),
                )
              ],
            ),
          ],
        ),
        AppIcon(
          name: "Sort",
          color: AppColors.get.primary,
        ),
      ],
    );
  }
}
