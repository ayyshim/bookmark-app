import 'package:app/di.dart';
import 'package:app/models/category.model.dart';
import 'package:app/models/item.model.dart';
import 'package:app/models/label.model.dart';
import 'package:app/services/category.service.dart';
import 'package:app/services/item.service.dart';
import 'package:app/views/view.model.dart';
import 'package:flutter/foundation.dart';
import 'package:fluttertoast/fluttertoast.dart';

class SingleCategoryModel extends ViewModel {
  final CategoryService _categoryService = locator<CategoryService>();
  final ItemService _itemService = locator<ItemService>();

  late List<LabelModel> _labels = [];
  List<LabelModel> get labels => _labels;

  late List<ItemModel> _items = [];
  List<ItemModel> get items => _items;

  late List<ItemModel> _itemsToShow = [];
  List<ItemModel> get itemsToShow => _itemsToShow;

  int _activeLabelIndex = 0;
  int get activeLabelIndex => _activeLabelIndex;

  late int _activeLabel =
      _labels.isNotEmpty ? _labels[_activeLabelIndex].id : 0;
  int get activeLabel => _activeLabel;

  int get totalBookmarks => _itemsToShow.length;

  Future<void> init(CategoryModel category) async {
    setLoading();
    try {
      _categoryService.setActiveCategory(category.id);
      _labels = await _itemService.getLabels();
      _items = await _itemService.getItems();
      _itemsToShow =
          _items.where((item) => item.labelId == _activeLabel).toList();

      debugPrint('${itemsToShow.length}');

      setIdle();
    } catch (e) {
      debugPrint(e.toString());
      Fluttertoast.showToast(
          msg: "Oops, That's awkward. Something went wrong!");
      setErrorMessage("Could not talk to the host server. See the logs.");
    }
  }

  Future<void> onLabelPressed(int value, int index) async {
    _activeLabelIndex = index;
    _activeLabel = value;
    _itemsToShow =
        _items.where((item) => item.labelId == _activeLabel).toList();
    setIdle();
  }

  Future<void> toggleFavoriteItem(ItemModel item) async {
    setWidgetBusy('item-favorite-${item.id}');
    try {
      final action = item.isFavorite ? Action.remove : Action.add;
      await _itemService.favorite(action, item: item);
      item.toggleFavorite();
    } catch (e) {
      debugPrint(e.toString());
      Fluttertoast.showToast(msg: "Something went wrong!");
    }
    unsetWidgetBusy('item-favorite-${item.id}');
  }

  Future<void> toggleBookmarkItem(ItemModel item) async {
    setWidgetBusy('item-bookmark-${item.id}');
    try {
      final action = item.isBookmarked ? Action.remove : Action.add;
      await _itemService.bookmark(action, item: item);
      item.toggleBookmark();
    } catch (e) {
      debugPrint(e.toString());
      Fluttertoast.showToast(msg: "Something went wrong!");
    }
    unsetWidgetBusy('item-bookmark-${item.id}');
  }

  Future<void> deleteItem(ItemModel item) async {
    setWidgetBusy('item-delete-${item.id}');
    try {
      await _itemService.deleteItem(item.id);
      _items.remove(item);
      _itemsToShow =
          _items.where((item) => item.labelId == _activeLabel).toList();
    } catch (e) {
      debugPrint(e.toString());
      Fluttertoast.showToast(msg: "Something went wrong!");
    }
    unsetWidgetBusy('item-delete-${item.id}');
  }
}
