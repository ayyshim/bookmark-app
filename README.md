# Bookmark App

Hello, My name is [Ashim Upadhaya](https://uashim.com.np) and I developed this app as an assement for the post of mid-level flutter developer at EatZiffy.

I have used provider with get_it for state management and have used my own assembled architecture. I have tried to make UI as pixel-perfect as I could but I found some inconsistency in figma UI so I have done some eye-balling while making UI. I have tweaked few things here and there on UI to make it look even more better. I have also added a `ErrorView` which will show if there is any error while fetching the data.

## How to run app

- Make sure your device is connected.
- Run the server.
- Goto `app` folder and run `flutter pub get` at first.
  > Make sure you have flutter v.2.x.x installed on your machine.
- Goto `app/lib/constants.dart` and change the `host`.
- For smooth UI experience, run app using `--release`.
  > `flutter run --release`
