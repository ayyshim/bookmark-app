# JOB Tasks
 

### You can find the Design here 
[Figma Task Design] (https://www.figma.com/file/7Ejntp18k9PympIaVltBUc/Intern-Project?node-id=97%3A130)


### Install JSON server globally for fake API's
* npm install -g json-server
* start the server using json-server --watch db.json
* List of all the endpoints needed for the app
    * http://localhost:3000/userDetails - For getting User Details
    * http://localhost:3000/categories - For the list of categories for home page
    * http://localhost:3000/items?catId={categoryId} for getting the list of items inside category (Page 2)
    * http://localhost:3000/labels?catId={categoryId} for getting the list of tags associated with the category (Page 2)
    * Filter the items as per the label selected using label id
    * Delete item from the list
        ** Delete Request http://localhost:3000/items/{itemId}
    * Update Favorite & Bookmark on tap
        ** Put Request http://localhost:3000/items/{itemId} just change favorite or bookmark value and everything else remains the same  
        {
        "image": "Image Value",
        "title": "Same as before",
        "isFavorite": true, // change value
        "isBookmarked": false, / change value
        "labelId": 6,
        "catId": 1,
        "id": 10
      }
